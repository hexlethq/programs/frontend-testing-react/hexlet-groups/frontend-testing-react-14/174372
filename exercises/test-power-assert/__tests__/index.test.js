const assert = require('power-assert');
const { flattenDepth } = require('lodash');

// BEGIN
const firstArray = [1, 2, [3]];
const firstFlattenArray = flattenDepth(firstArray);

const secondArray = [1, 2, [3, [4, 5]]];
const secondFlattenArray = flattenDepth(secondArray);
const secondFlattenDeepArray = flattenDepth(secondArray, 2);
const filteredSecondFlattenDeepArray = secondFlattenDeepArray.filter((el) => Array.isArray(el));

const thirdArray = [1, [2, [3, [4, [5]]]]];
const thirdFlattenArray = flattenDepth(thirdArray, Infinity);
const filteredThirdFlattenArray = thirdFlattenArray.filter((el) => Array.isArray(el));
const thirdArrayNotFlattened = flattenDepth(thirdArray, 'hello, world!');

assert(firstFlattenArray[2] === 3);
assert.deepEqual([1, 2, 3], firstFlattenArray);

assert(secondFlattenArray[2] === 3);
assert(Array.isArray(secondFlattenArray[3]) === true);
assert(secondFlattenArray[3][0] === 4);
assert.deepEqual(secondFlattenArray[3], [4, 5]);

assert(secondFlattenDeepArray[4] === 5);
assert(filteredSecondFlattenDeepArray.length === 0);

assert(filteredThirdFlattenArray.length === 0);
assert(Array.isArray(thirdArrayNotFlattened[1]) === true);
assert.deepEqual(thirdArray, thirdArrayNotFlattened);
// END

// ПЕРЕД ТЕМ КАК НАЧАТЬ РЕШАТЬ ЗАДАНИЕ
// УДАЛИТЕ СЛЕДУЮЩУЮ СТРОКУ
