const fc = require('fast-check');

const sort = (data) => data.slice().sort((a, b) => a - b);

// BEGIN
test('should be the same length', () => {
  fc.assert(
    fc.property(fc.array(fc.integer()), (data) => {
      const sorted = sort(data);
      expect(sorted.length).toEqual(data.length);
    }),
  );
  fc.assert(
    fc.property(fc.array(fc.hexa()), (data) => {
      const sorted = sort(data);
      expect(sorted.length).toEqual(data.length);
    }),
  );
});

test('should contain the same elements', () => {
  fc.assert(
    fc.property(fc.array(fc.integer()), (data) => {
      const sorted = sort(data);
      expect(sorted).toEqual(expect.arrayContaining(data));
    }),
  );
  fc.assert(
    fc.property(fc.array(fc.hexa()), (data) => {
      const sorted = sort(data);
      expect(sorted).toEqual(expect.arrayContaining(data));
    }),
  );
});

test('should be idempotent', () => {
  fc.assert(
    fc.property(fc.array(fc.integer()), (data) => {
      const sorted1 = sort(data);
      const sorted2 = sort(data);
      expect(sorted1).toStrictEqual(sorted2);
    }),
  );
  fc.assert(
    fc.property(fc.array(fc.hexa()), (data) => {
      const sorted1 = sort(data);
      const sorted2 = sort(data);
      expect(sorted1).toStrictEqual(sorted2);
    }),
  );
});

test('should be sorted', () => {
  fc.assert(
    fc.property(fc.array(fc.integer()), (data) => {
      const sorted = sort(data);
      expect(sorted).toBeSorted();
    }),
  );
  fc.assert(
    fc.property(fc.array(fc.float()), (data) => {
      const sorted = sort(data);
      expect(sorted).toBeSorted();
    }),
  );
});
// END
