// BEGIN
const timeout = 30000;

describe('Test blog application', () => {
  beforeAll(async () => {
    await page.goto('http://localhost:5000/', { waitUntil: 'domcontentloaded' });
  });

  test('Should open home page', async () => {
    const currentUrl = await page.url();

    expect(currentUrl).toEqual('http://localhost:5000/');
  }, timeout);

  test('Should open article page with articles', async () => {
    const articlesLinkSelector = '[data-testid="nav-articles-index-link"]';
    const tableSelector = '.table';

    await page.click(articlesLinkSelector);
    await page.waitForSelector(articlesLinkSelector, { visible: true });
    const currentUrl = await page.url();
    const response = await page.waitForSelector(tableSelector, { visible: true });
    const { _remoteObject: remoteObject } = response;
    const { className } = remoteObject;

    expect(currentUrl).toEqual('http://localhost:5000/articles');
    expect(className).toEqual('HTMLTableElement');
  }, timeout);

  test('Should open create page with form', async () => {
    const formSelector = 'form';
    const articleCreateLinkSelector = '[data-testid="article-create-link"]';

    await page.click(articleCreateLinkSelector);
    const response = await page.waitForSelector(formSelector, { visible: true });
    const currentUrl = await page.url();
    const { _remoteObject: remoteObject } = response;
    const { className } = remoteObject;

    expect(currentUrl).toEqual('http://localhost:5000/articles/new');
    expect(className).toEqual('HTMLFormElement');
  }, timeout);

  test('Should fill form and create article, then redirect to articles page with new post in table', async () => {
    const createButtonSelector = '[data-testid="article-create-button"]';
    const newArticleTitle = 'Cool post';
    const newArticleContent = 'Cool content';
    const newPostSelector = 'body > div.container.mt-3 > table > tbody > tr:nth-child(5) > td:nth-child(2)';

    await page.type('#name', newArticleTitle);
    await page.select('#category', '2');
    await page.type('#content', newArticleContent);
    await page.click(createButtonSelector);
    const response = await page.goto('http://localhost:5000/articles');
    const currentUrl = await page.url();
    const { _status: status } = response;
    const newPost = await page.waitForSelector(newPostSelector, { visible: true });
    const { _remoteObject: remoteObject } = newPost;
    const { className } = remoteObject;
    const newPostTitle = await page.$eval(newPostSelector, (element) => element.innerHTML);

    expect(currentUrl).toEqual('http://localhost:5000/articles');
    expect(status).toEqual(200);
    expect(className).toEqual('HTMLTableCellElement');
    expect(newPostTitle).toEqual(newArticleTitle);
  }, timeout);

  test('Should edit new article', async () => {
    const editButtonSelector = '[data-testid="article-edit-link-8"]';
    const updateButtonSelector = '[data-testid="article-update-button"]';
    const editedArticleTitle = 'Very ';
    const updatedPostSelector = 'body > div.container.mt-3 > table > tbody > tr:nth-child(5) > td:nth-child(2)';

    await page.click(editButtonSelector);
    await page.goto('http://localhost:5000/articles/8/edit');
    await page.type('#name', editedArticleTitle);
    await page.click(updateButtonSelector);
    await page.goto('http://localhost:5000/articles');
    const updatedPostTitle = await page.$eval(updatedPostSelector, (element) => element.innerHTML);

    expect(updatedPostTitle).toEqual('Very Cool post');
  }, timeout);
});
// END
