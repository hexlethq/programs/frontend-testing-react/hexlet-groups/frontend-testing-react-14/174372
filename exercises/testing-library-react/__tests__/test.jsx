/* eslint-disable max-len */
// @ts-nocheck

import '@testing-library/jest-dom';

import nock from 'nock';
import React from 'react';
import { render, screen, waitFor } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import Autocomplete from '../src/Autocomplete.jsx';

const host = 'http://localhost';

beforeAll(() => {
  nock.disableNetConnect();
});

// BEGIN
const firstLetterCountries = ['Ecuador', 'Estonia', 'Egypt', 'Eritrea', 'Ethiopia', 'Equatorial Guinea', 'El Salvador', 'Eswatini'];
const secondLetterCountries = ['Estonia', 'Eswatini'];
const thirdLetterCountries = ['Eswatini'];

beforeEach(() => {
  render(<Autocomplete />);
});

describe('Autocomplete testing', () => {
  it('should display autocomplete properly', async () => {
    const firstUserInput = 'E';
    const inputField = screen.getByRole('textbox');
    userEvent.type(inputField, firstUserInput);
    const firstScope = nock(host).get('/countries').query({ term: 'E' }).reply(200, firstLetterCountries);
    await waitFor(() => {
      const firstRequestIsPerformed = firstScope.isDone();
      expect(firstRequestIsPerformed).toBe(true);
    });

    const firstAutocompletedCountries = await screen.findByRole('list');
    const firstListItems = Array.from(await screen.findAllByRole('listitem'));
    expect(firstAutocompletedCountries).toContainElement(screen.getByText(firstLetterCountries[0]));
    expect(firstListItems.length).toEqual(firstLetterCountries.length);

    const seconduserInput = 's';
    userEvent.type(inputField, seconduserInput);
    const secondScope = nock(host).get('/countries').query({ term: 'Es' }).reply(200, secondLetterCountries);
    await waitFor(() => {
      const secondRequestIsPerformed = secondScope.isDone();
      expect(secondRequestIsPerformed).toBe(true);
    });

    const secondAutocompletedCountries = await screen.findByRole('list');
    const secondListItems = Array.from(await screen.findAllByRole('listitem'));
    expect(secondAutocompletedCountries).toContainElement(screen.getByText(secondLetterCountries[0]));
    expect(secondListItems.length).toEqual(secondLetterCountries.length);

    const thirdUserInput = 'w';
    userEvent.type(inputField, thirdUserInput);
    const thirdScope = nock(host).get('/countries').query({ term: 'Esw' }).reply(200, thirdLetterCountries);
    await waitFor(() => {
      const thirdRequestIsPerformed = thirdScope.isDone();
      expect(thirdRequestIsPerformed).toBe(true);
    });
    const thirdAutocompletedCountries = await screen.findByRole('list');
    const thirdListItems = Array.from(await screen.findAllByRole('listitem'));
    expect(thirdAutocompletedCountries).toContainElement(screen.getByText(thirdLetterCountries[0]));
    expect(thirdListItems.length).toEqual(thirdLetterCountries.length);

    userEvent.clear(inputField);
    const listItemsAfterClean = Array.from(screen.findAllByRole('listitem'));
    expect(listItemsAfterClean.length).toEqual(0);
  });
});
// END
