test('main', () => {
  const src = { k: 'v', b: 'b' };
  const target = { k: 'v2', a: 'a' };
  const result = Object.assign(target, src);

  expect.hasAssertions();

  // BEGIN
  expect(result.k).toBe('v');
  expect(result.a).toBe('a');
  expect(result.b).toBe('b');
  expect(result).toHaveProperty('b');
  expect(result).not.toHaveProperty('z');
  expect(result.k).toBeDefined();
  expect(result).toBe(target);
  // END
});
