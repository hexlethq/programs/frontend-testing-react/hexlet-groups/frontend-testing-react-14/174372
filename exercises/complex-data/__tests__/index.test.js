const faker = require('faker');

// BEGIN
test('date property should be date', () => {
  const transaction = faker.helpers.createTransaction();

  expect(transaction.date).toEqual(expect.any(Date));
});

test('name property should be defined', () => {
  const transaction = faker.helpers.createTransaction();

  expect(transaction.name).toBeDefined();
});

test('should contain all properties', () => {
  const transaction = faker.helpers.createTransaction();

  expect(Object.keys(transaction)).toMatchObject(expect.arrayContaining(['amount', 'account', 'business', 'date', 'name', 'type']));
});

test('should generate unique transactions', () => {
  const firstTransaction = faker.helpers.createTransaction();
  const secondTransaction = faker.helpers.createTransaction();

  expect(firstTransaction).not.toMatchObject(secondTransaction);
});
// END
