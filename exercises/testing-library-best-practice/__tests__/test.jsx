// @ts-nocheck

import React from 'react';
import nock from 'nock';
import axios from 'axios';
import httpAdapter from 'axios/lib/adapters/http';
import { render, screen, waitFor } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import '@testing-library/jest-dom';

import TodoBox from '../src/TodoBox.jsx';

axios.defaults.adapter = httpAdapter;
const host = 'http://localhost';

beforeAll(() => {
  nock.disableNetConnect();
});

afterAll(() => {
  nock.enableNetConnect();
});

beforeEach(() => {
  nock(host).get('/tasks').reply(200, []);
});

// BEGIN
it('should display only input and button in initial state', () => {
  render(<TodoBox />);
  const listElement = screen.queryByRole('link');
  const button = screen.getByRole('button');
  const input = screen.getByRole('textbox');

  expect(listElement).not.toBeInTheDocument();
  expect(button).toBeInTheDocument();
  expect(input).toBeInTheDocument();
});

it('should display added tasks', async () => {
  render(<TodoBox />);
  const firstTask = {
    id: 1,
    text: 'first task',
    state: 'active',
  };
  const secondTask = {
    id: 2,
    text: 'second task',
    state: 'active',
  };
  const button = screen.getByRole('button');
  const input = screen.getByRole('textbox');

  userEvent.type(input, firstTask.text);
  userEvent.click(button);
  const addFirstTaskScope = nock(host).post('/tasks', { text: firstTask.text }).reply(200, firstTask);
  await waitFor(() => {
    const addFirstTaskScopeIsDone = addFirstTaskScope.isDone();
    expect(addFirstTaskScopeIsDone).toBe(true);
  });
  const firstTaskElement = await screen.findByRole('link');
  expect(firstTaskElement).toBeInTheDocument();

  userEvent.type(input, secondTask.text);
  userEvent.click(button);
  const addSecondTaskScope = nock(host).post('/tasks', { text: secondTask.text }).reply(200, secondTask);
  await waitFor(() => {
    const addSecondTaskScopeIsDone = addSecondTaskScope.isDone();
    expect(addSecondTaskScopeIsDone).toBe(true);
  });

  const tasksElements = await screen.findAllByRole('link');
  expect(tasksElements.length).toBe(2);
});

it('should complete task and allow to reverse completion', async () => {
  const { container } = render(<TodoBox />);
  const task = {
    id: 1,
    text: 'my task',
    state: 'active',
  };
  const button = screen.getByRole('button');
  const input = screen.getByRole('textbox');

  userEvent.type(input, task.text);
  userEvent.click(button);
  const addTaskScope = nock(host).post('/tasks', { text: task.text }).reply(200, task);
  await waitFor(() => {
    const addTaskScopeIsDone = addTaskScope.isDone();
    expect(addTaskScopeIsDone).toBe(true);
  });
  const taskElement = await screen.findByRole('link');
  expect(taskElement).toBeInTheDocument();
  const activeTasksWrapper = container.querySelector('.todo-active-tasks');
  expect(activeTasksWrapper).toBeInTheDocument();

  userEvent.click(taskElement);
  const completeTaskScope = nock(host).patch(`/tasks/${task.id}/finish`).reply(200, { ...task, state: 'finished' });
  await waitFor(() => {
    const completeTaskScopeIsDone = completeTaskScope.isDone();
    expect(completeTaskScopeIsDone).toBe(true);
  });
  const finishedTasksWrapper = container.querySelector('.todo-finished-tasks');
  expect(finishedTasksWrapper).toBeInTheDocument();

  const finishedTaskElement = await screen.findByText(task.text);
  userEvent.click(finishedTaskElement);
  const reactivateTaskScope = nock(host).patch(`/tasks/${task.id}/activate`).reply(200, { ...task, state: 'active' });
  await waitFor(() => {
    const reactivateTaskScopeIsDone = reactivateTaskScope.isDone();
    expect(reactivateTaskScopeIsDone).toBe(true);
  });
  expect(finishedTasksWrapper).not.toBeInTheDocument();
});
// END
