const nock = require('nock');
const axios = require('axios');
const { get, post } = require('../src/index.js');

axios.defaults.adapter = require('axios/lib/adapters/http');
// BEGIN
const getUrl = 'https://httpbin.org/get';
const postUrl = 'https://httpbin.org/post';

beforeAll(() => {
  nock.disableNetConnect();
});

afterAll(() => {
  nock.cleanAll();
  nock.enableNetConnect();
});

test('get', async () => {
  const data = {
    successMessage: 'Hello, World!',
  };

  const scope = nock('https://httpbin.org').get('/get').reply(200, data);
  const result = await get(getUrl);

  expect(scope.isDone()).toBeTruthy();
  expect(result.successMessage).toBe(data.successMessage);
});

test('post', async () => {
  const data = {
    json: { age: 33, firstname: 'Fedor', lastname: 'Sumkin' },
  };

  const scope = nock('https://httpbin.org').post('/post').reply(200, data);
  const { json } = await post(postUrl);

  expect(json).toEqual(data.json);
  expect(scope.isDone()).toBeTruthy();
});

test('error in get', async () => {
  nock('https://httpbin.org').get('/get').replyWithError('400');
  const result = await get(getUrl);

  expect(result).toBe('400');
});

test('error in post', async () => {
  nock('https://httpbin.org').post('/post').replyWithError('500');
  const result = await post(postUrl);

  expect(result).toBe('500');
});
// END
