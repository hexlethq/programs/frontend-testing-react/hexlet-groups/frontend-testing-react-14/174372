const axios = require('axios');

// BEGIN
const get = (url) => {
  const result = axios.get(url)
    .then((response) => response.data)
    .then((data) => data)
    .catch((err) => err.message);

  return result;
};

const post = (url) => {
  const result = axios.post(url, {
    firstname: 'Fedor',
    lastname: 'Sumkin',
    age: 33,
  })
    .then((response) => response.data)
    .then((data) => data)
    .catch((err) => err.message);

  return result;
};

// END

module.exports = { get, post };
