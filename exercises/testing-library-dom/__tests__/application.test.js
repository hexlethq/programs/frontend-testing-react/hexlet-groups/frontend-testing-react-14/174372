// @ts-nocheck

require('@testing-library/jest-dom');
const fs = require('fs');
const path = require('path');
const testingLibraryDom = require('@testing-library/dom');
const testingLibraryUserEvent = require('@testing-library/user-event');

const run = require('../src/application');

const { screen } = testingLibraryDom;
const userEvent = testingLibraryUserEvent.default;

beforeEach(() => {
  const initHtml = fs.readFileSync(path.join('__fixtures__', 'index.html')).toString();
  document.body.innerHTML = initHtml;
  run();
});

// BEGIN
test('Should display one list and no tasks', () => {
  const defaultList = screen.queryByText(/General/i);
  const lists = document.querySelectorAll('[data-container="lists"] > ul > li');
  const listsArray = Array.from(lists);
  const tasksContainer = document.querySelector('[data-container="tasks"]');

  expect(defaultList).toHaveTextContent('General');
  expect(listsArray.length).toBe(1);
  expect(tasksContainer.innerHTML).toBeFalsy();
});

test('Should add task to default list', () => {
  const addTaskInput = screen.getByRole('textbox', { name: /new task name/i });
  const addTaskButton = screen.getByRole('button', { name: /add task/i });
  const lists = document.querySelectorAll('[data-container="lists"] > ul > li');
  const listsArray = Array.from(lists);
  const firstTask = 'First task';

  userEvent.type(addTaskInput, firstTask);
  userEvent.click(addTaskButton);

  const task = document.querySelector('[data-container="tasks"] > ul > li');

  expect(task.innerHTML).toEqual(firstTask);
  expect(listsArray.length).toBe(1);
});

test('Should add and display task and clean input', () => {
  const addTaskInput = screen.getByRole('textbox', { name: /new task name/i });
  const task = 'To drink tea';
  const addTaskButton = screen.getByRole('button', { name: /add task/i });
  const beforeInputValue = addTaskInput.value;

  userEvent.type(addTaskInput, task);
  const filledInputValue = addTaskInput.value;
  userEvent.click(addTaskButton);

  const afterInputValue = addTaskInput.value;
  const addedTask = screen.queryByText(task);

  expect(beforeInputValue).not.toEqual(filledInputValue);
  expect(beforeInputValue).toBeFalsy();
  expect(filledInputValue).toEqual(task);
  expect(afterInputValue).toBeFalsy();
  expect(addedTask).toBeVisible();
});

test('Should add and display multiple tasks', () => {
  const addTaskInput = screen.getByRole('textbox', { name: /new task name/i });
  const firstTask = 'To read book';
  const secondTask = 'To write book';
  const addTaskButton = screen.getByRole('button', { name: /add task/i });

  userEvent.type(addTaskInput, firstTask);
  userEvent.click(addTaskButton);
  userEvent.type(addTaskInput, secondTask);
  userEvent.click(addTaskButton);

  const firstAddedTask = screen.queryByText(firstTask);
  const secondAddedTask = screen.queryByText(secondTask);

  expect(firstAddedTask).toBeVisible();
  expect(secondAddedTask).toBeVisible();
});

test('Should add task to default list, should and new list and switch between lists', () => {
  const addTaskInput = screen.getByRole('textbox', { name: /new task name/i });
  const firstTask = 'To read book';
  const addTaskButton = screen.getByRole('button', { name: /add task/i });
  const addListInput = screen.getByRole('textbox', { name: /new list/i });
  const newList = 'New list';
  const addListButton = screen.getByRole('button', { name: /add list/i });

  userEvent.type(addTaskInput, firstTask);
  userEvent.click(addTaskButton);

  const firstAddedTask = screen.queryByText(firstTask);

  userEvent.type(addListInput, newList);
  userEvent.click(addListButton);
  const addedList = screen.queryByText(newList);
  userEvent.click(addedList);
  const firstAddedTaskAfterListSwitch = screen.queryByText(firstTask);

  expect(firstAddedTask.innerHTML).toBe(firstTask);
  expect(addedList.innerHTML).toBe(newList);
  expect(firstAddedTaskAfterListSwitch).toBeFalsy();
});

test('Should add task to second list', () => {
  const addTaskInput = screen.getByRole('textbox', { name: /new task name/i });
  const task = 'Task for second list';
  const addTaskButton = screen.getByRole('button', { name: /add task/i });
  const addListInput = screen.getByRole('textbox', { name: /new list/i });
  const newList = 'New list';
  const addListButton = screen.getByRole('button', { name: /add list/i });

  userEvent.type(addListInput, newList);
  userEvent.click(addListButton);
  const addedList = screen.queryByText(newList);
  userEvent.click(addedList);
  userEvent.type(addTaskInput, task);
  userEvent.click(addTaskButton);
  const addedTask = screen.queryByText(task);
  const newListAfterSwitchingLists = screen.queryByText(newList);

  expect(addedTask).toBeVisible();
  expect(newListAfterSwitchingLists).toBeVisible();
});
// END
