const fs = require('fs');

// BEGIN
const upVersion = (file, param = 'patch') => {
  if (!fs.existsSync(file)) {
    throw new Error('file does not exist, check filepath again, please');
  }

  const content = fs.readFileSync(file, 'utf-8');
  const parsed = JSON.parse(content);
  const { version } = parsed;
  const splitted = version.split('.');
  const numerized = splitted.map((el) => parseInt(el, 10));

  const versionMap = {
    major: numerized[0],
    minor: numerized[1],
    patch: numerized[2],
  };

  if (param === 'patch') {
    versionMap.patch += 1;
    if (versionMap.patch === 10) {
      versionMap.patch = 0;
      versionMap.minor += 1;
      if (versionMap.minor === 10) {
        versionMap.minor = 0;
        versionMap.major += 1;
      }
    }
  }

  if (param === 'minor') {
    versionMap.minor += 1;
    versionMap.patch = 0;
    if (versionMap.minor === 10) {
      versionMap.patch = 0;
      versionMap.minor = 0;
      versionMap.major += 1;
    }
  }

  if (param === 'major') {
    versionMap.major += 1;
    versionMap.minor = 0;
    versionMap.patch = 0;
  }

  const newVersion = { version: `${versionMap.major}.${versionMap.minor}.${versionMap.patch}` };

  return fs.writeFileSync(file, JSON.stringify(newVersion));
};

// END

module.exports = { upVersion };
