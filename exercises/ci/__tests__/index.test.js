const fs = require('fs');
const path = require('path');
const { upVersion } = require('../src/index.js');

// BEGIN
const getFixturePath = (name) => path.join('__fixtures__', name);
const fileName = 'package.json';
const src = getFixturePath(fileName);

let actual;
beforeAll(() => {
  actual = fs.readFileSync(getFixturePath('package.json'), 'utf-8');
});

afterEach(() => {
  fs.writeFileSync(src, JSON.stringify({ version: '1.3.2' }));
});

test('should change version file', () => {
  upVersion(src);
  const changedContent = fs.readFileSync(src, 'utf-8');

  expect(JSON.parse(actual)).not.toMatchObject(JSON.parse(changedContent));
});

test('should change patch version with default argument', () => {
  upVersion(src);
  const changedContent = fs.readFileSync(src, 'utf-8');
  const changedObject = JSON.parse(changedContent);
  const { version: changedVersion } = changedObject;

  expect(changedVersion).toEqual('1.3.3');
});

test('should change patch version', () => {
  upVersion(src, 'patch');
  const changedContent = fs.readFileSync(src, 'utf-8');
  const changedObject = JSON.parse(changedContent);
  const { version: changedVersion } = changedObject;

  expect(changedVersion).toEqual('1.3.3');
});

test('should change minor version', () => {
  upVersion(src, 'minor');
  const changedContent = fs.readFileSync(src, 'utf-8');
  const changedObject = JSON.parse(changedContent);
  const { version: changedVersion } = changedObject;

  expect(changedVersion).toEqual('1.4.0');
});

test('should change major version', () => {
  upVersion(src, 'major');
  const changedContent = fs.readFileSync(src, 'utf-8');
  const changedObject = JSON.parse(changedContent);
  const { version: changedVersion } = changedObject;

  expect(changedVersion).toEqual('2.0.0');
});

test('should throw with wrong path', () => {
  expect(() => upVersion('/tmp/wrongpath')).toThrow('file does not exist, check filepath again, please');
});
// END
