// @ts-nocheck

require('@testing-library/jest-dom');
const fs = require('fs');
const path = require('path');

const run = require('../src/application');

beforeEach(() => {
  const initHtml = fs.readFileSync(path.join('__fixtures__', 'index.html')).toString();
  document.body.innerHTML = initHtml;
  run();
});

// BEGIN
test('Initial HTML should have one default list and no tasks', () => {
  const listOfLists = document.querySelectorAll('[data-container="lists"] > ul > li');
  const lists = Array.from(listOfLists);

  const tasks = document.querySelector('[data-container="tasks"]');

  expect(lists.length).toEqual(1);
  expect(tasks).toBeEmptyDOMElement();
});

test('Should add task to default list', () => {
  const firstTask = 'First task';
  const listOfLists = document.querySelectorAll('[data-container="lists"] > ul > li');
  const lists = Array.from(listOfLists);

  const tasks = document.querySelector('[data-container="tasks"]');
  const addButton = document.querySelector('[data-testid="add-task-button"]');
  const addTaskInput = document.querySelector('[data-testid="add-task-input"]');
  addTaskInput.setAttribute('value', firstTask);
  addButton.click();

  const task = document.querySelector('[data-container="tasks"] ul li');
  const generalList = lists[0];

  expect(tasks).not.toBeEmptyDOMElement();
  expect(lists.length).toEqual(1);
  expect(task.innerHTML).toEqual(firstTask);
  expect(generalList).toContainHTML('<li><b>General</b></li>');
});

test('Should add task and clean input', () => {
  const taskText = 'To do hometask';

  const addTaskInput = document.querySelector('[data-testid="add-task-input"]');
  const addButton = document.querySelector('[data-testid="add-task-button"]');
  addTaskInput.setAttribute('value', taskText);
  addButton.click();
  addTaskInput.setAttribute('value', '');

  const addedTask = document.querySelector('[data-container="tasks"] ul li');

  expect(addedTask.innerHTML).toEqual(taskText);
  expect(addTaskInput.value).toBe('');
});

test('Should add second task and display two tasks in the list', () => {
  const firstTask = 'To sing song';
  const secondTask = 'To play game';

  const addTaskInput = document.querySelector('[data-testid="add-task-input"]');
  const addButton = document.querySelector('[data-testid="add-task-button"]');
  addTaskInput.setAttribute('value', firstTask);
  addButton.click();
  addTaskInput.setAttribute('value', '');
  addTaskInput.setAttribute('value', secondTask);
  addButton.click();

  const tasksElements = document.querySelectorAll('[data-container="tasks"] ul li');
  const tasks = Array.from(tasksElements);

  expect(tasks.length).toBe(2);
  expect(tasks[0].innerHTML).toEqual(firstTask);
  expect(tasks[1].innerHTML).toEqual(secondTask);
});

test('Should add second taskslist, should display tasks by lists', () => {
  const addTaskInput = document.querySelector('[data-testid="add-task-input"]');
  const addTaskButton = document.querySelector('[data-testid="add-task-button"]');
  const addListInput = document.querySelector('[data-testid="add-list-input"]');
  const addListButton = document.querySelector('[data-testid="add-list-button"]');
  const secondList = 'Second';
  const firstListTask = 'First list task';
  const secondListTask = 'Second list task';

  addListInput.setAttribute('value', secondList);
  addListButton.click();
  const listOfLists = document.querySelectorAll('[data-container="lists"] > ul > li');
  const lists = Array.from(listOfLists);
  const secondListItem = document.querySelector('[data-testid="second-list-item"]');

  addTaskInput.setAttribute('value', firstListTask);
  addTaskButton.click();
  addTaskInput.setAttribute('value', '');
  secondListItem.click();
  addTaskInput.setAttribute('value', secondListTask);
  addTaskButton.click();

  const tasksElements = document.querySelectorAll('[data-container="tasks"] ul li');
  const tasks = Array.from(tasksElements);
  const secondTaskElement = tasks[0];
  const generalListItem = document.querySelector('[data-testid="general-list-item"]');
  generalListItem.click();
  const generalListTasksElements = document.querySelectorAll('[data-container="tasks"] ul li');
  const generalListTasks = Array.from(generalListTasksElements);
  const generalListTaskElement = generalListTasks[0];

  expect(lists.length).toEqual(2);
  expect(secondListItem.innerHTML).toBe(secondList);
  expect(tasks.length).toBe(1);
  expect(secondTaskElement.innerHTML).toBe(secondListTask);
  expect(generalListTasks.length).toBe(1);
  expect(generalListTaskElement.innerHTML).toBe(firstListTask);
});

test('should add task to second list', () => {
  const addTaskInput = document.querySelector('[data-testid="add-task-input"]');
  const addTaskButton = document.querySelector('[data-testid="add-task-button"]');
  const addListInput = document.querySelector('[data-testid="add-list-input"]');
  const addListButton = document.querySelector('[data-testid="add-list-button"]');
  const secondList = 'Second';
  const secondListTask = 'Second list task';

  addListInput.setAttribute('value', secondList);
  addListButton.click();

  const secondListItem = document.querySelector('[data-testid="second-list-item"]');
  secondListItem.click();
  addTaskInput.setAttribute('value', secondListTask);
  addTaskButton.click();

  const tasksElements = document.querySelectorAll('[data-container="tasks"] ul li');
  const tasks = Array.from(tasksElements);
  const taskElement = tasks[0];

  expect(taskElement.innerHTML).toEqual(secondListTask);
});
// END
