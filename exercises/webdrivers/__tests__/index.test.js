const puppeteer = require('puppeteer');
const getApp = require('../server/index.js');

const port = 5001;
const appUrl = `http://localhost:${port}`;
const timeout = '30000';
jest.setTimeout(timeout);

let browser;
let page;

const app = getApp();

describe('it works', () => {
  beforeAll(async () => {
    await app.listen(port, '0.0.0.0');
    browser = await puppeteer.launch({
      args: ['--no-sandbox'],
      headless: true,
      // slowMo: 250
    });
    page = await browser.newPage();
    await page.setViewport({
      width: 1280,
      height: 720,
    });
  });
  // BEGIN
  test('should open page', async () => {
    const response = await page.goto(appUrl);
    const { _status: status } = response;

    expect(status).toEqual(200);
  });

  test('should open articles page', async () => {
    const articlesLinkSelector = 'a.nav-link';

    await page.click(articlesLinkSelector);
    const currentUrl = await page.url();

    expect(currentUrl).toEqual('http://localhost:5001/articles');
  });

  test('articles page should have table of articles', async () => {
    const tableSelector = '#articles';
    await page.goto(`${appUrl}/articles`);
    const response = await page.waitForSelector(tableSelector, { visible: true });
    const { _remoteObject: remoteObject } = response;
    const { className } = remoteObject;

    expect(className).toEqual('HTMLTableElement');
  });

  test('should open create article page with form', async () => {
    await page.goto(`${appUrl}/articles`);
    await page.click('a[href="/articles/new"]');
    const currentUrl = await page.url();
    const response = await page.waitForSelector('form', { visible: true });
    const { _remoteObject: remoteObject } = response;
    const { className } = remoteObject;

    expect(currentUrl).toEqual('http://localhost:5001/articles/new');
    expect(className).toEqual('HTMLFormElement');
  });

  test('should create new article', async () => {
    const newArticleTitle = 'Cool post';
    const newArticleContent = 'Cool content';
    await page.goto(`${appUrl}/articles`);
    await page.click('a[href="/articles/new"]');
    await page.goto(`${appUrl}/articles/new`);
    await page.type('#name', newArticleTitle);
    await page.select('#category', '2');
    await page.type('#content', newArticleContent);
    await page.click('.btn');
    await page.goto(`${appUrl}/articles`);
    const currentUrl = await page.url();
    const response = await page.goto(currentUrl);
    const { _status: status } = response;
    const newPost = await page.waitForSelector('#articles > tbody > tr:nth-child(5) > td:nth-child(2)', { visible: true });
    const { _remoteObject: remoteObject } = newPost;
    const { className } = remoteObject;

    expect(currentUrl).toEqual(`${appUrl}/articles`);
    expect(status).toEqual(200);
    expect(className).toEqual('HTMLTableCellElement');
  });

  test('should edit article', async () => {
    const newArticleTitle = 'Second Cool post';
    const newArticleContent = 'Another Cool content';
    const editedArticleTitle = 'First ';
    await page.goto(`${appUrl}/articles`);
    await page.click('a[href="/articles/new"]');
    await page.goto(`${appUrl}/articles/new`);
    await page.type('#name', newArticleTitle);
    await page.select('#category', '2');
    await page.type('#content', newArticleContent);
    await page.click('.btn');
    await page.goto(`${appUrl}/articles`);
    await page.click('#articles > tbody > tr:nth-child(5) > td:nth-child(4) > a');
    await page.goto(`${appUrl}/articles/8/edit`);
    await page.type('#name', editedArticleTitle);
    await page.click('.btn');
    await page.goto(`${appUrl}/articles`);
    const updatedTitle = await page.$eval('#articles > tbody > tr:nth-child(5) > td:nth-child(2)', (element) => element.innerHTML);

    expect(updatedTitle).toEqual('First Cool post');
  });
  // END
  afterAll(async () => {
    await browser.close();
    await app.close();
  });
});
