// @ts-nocheck
// BEGIN
const appUrl = 'http://0.0.0.0:3000/';
const timeout = 30000;

describe('Test ToDo application', () => {
  beforeEach(async () => {
    await page.goto(appUrl, { waitUntil: 'domcontentloaded' });
  });

  test('applications opens, has input and button', async () => {
    const inputSelector = 'input[data-testid="task-name-input"]';
    const buttonSelector = 'input[data-testid="add-task-button"]';
    const currentUrl = await page.url();

    expect(currentUrl).toEqual(appUrl);
    await expect(page).toMatchElement(inputSelector);
    await expect(page).toMatchElement(buttonSelector);
  });

  test('Should add task', async () => {
    const inputSelector = 'input[data-testid="task-name-input"]';
    const buttonSelector = 'input[data-testid="add-task-button"]';
    const addedTaskSelector = '.list-group-item > span';
    const importantTask = 'To burn MSW';

    await page.type(inputSelector, importantTask);
    await page.click(buttonSelector);
    await page.waitForSelector(addedTaskSelector, { visible: true });

    await expect(page).toMatch(importantTask);
  });

  test('Should remove task', async () => {
    const inputSelector = 'input[data-testid="task-name-input"]';
    const buttonSelector = 'input[data-testid="add-task-button"]';
    const addedTaskSelector = '.list-group-item > span';
    const newTask = 'To drink tea';
    const removeButtonSelector = '[data-testid="remove-task-1"]';

    await page.type(inputSelector, newTask);
    await page.click(buttonSelector);
    await page.waitForSelector(addedTaskSelector, { visible: true });

    await page.click(removeButtonSelector);
    await page.waitForSelector(buttonSelector, { visible: true });

    await expect(page).not.toMatch(newTask);
  }, timeout);
});

// END
